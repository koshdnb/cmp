(function() {
    'use strict';

    /**
     * @name  config
     * @description config block
     */
    function config($stateProvider) {
        $stateProvider
            .state('root.carousel', {
                abstract: true,
                url: '/carousel',
                views: {
                    '@': {
                        templateUrl: 'app/partials/content.tpl.html',
                        controller: 'carouselCtrl as carousel'
                    }
                }
            })
            .state('root.carousel.page', {
                url: '',
                views: {
                    'content@root.carousel': {
                        templateUrl: 'app/carousel/carousel.tpl.html'
                    },
                    'runtime@root.carousel': {
                        templateUrl: 'app/carousel/runC.tpl.html'
                    }
                }
            });
    }

    /**
     * @name  carouselCtrl
     * @description Controller
     */
    function carouselCtrl($scope) {
        var vm = this;
        $scope.wert = function() {
            console.log(arguments);
        };
        $scope.index = 0;

        $scope.array = [
            { item: 10 },
            { item: 20 },
            { item: 30 },
            { item: 40 },
            { item: 50 },
            { item: 60 },
            { item: 70 }
        ];

        $scope.$watch('nextCallbackEnabled', function(newVal) {
            if (newVal) {
                $scope.nextDesc = "Print 'fizz' on even, 'buzz' on odd:";
                $scope.nextCallback = function() {
                    $scope.nextVar = $scope.index % 2 ? 'buzz' : 'fizz';
                }
            } else {
                $scope.nextDesc = "Print the index:";
                $scope.nextCallback = function() {
                    $scope.nextVar = $scope.index;
                }
            }
        });

        $scope.$watch('prevCallbackEnabled', function(newVal) {
            if (newVal) {
                $scope.prevDesc = "Print 'fizz' on even, 'buzz' on odd:";
                $scope.prevCallback = function() {
                    $scope.prevVar = $scope.index % 2 ? 'buzz' : 'fizz';
                }
            } else {
                $scope.prevDesc = "Print the index:";
                $scope.prevCallback = function() {
                    $scope.prevVar = $scope.index;
                }
            }
        });
    }

    angular.module('app')
        .config(config)
        .controller('carouselCtrl', carouselCtrl);
})();
