(function() {
  'use strict';

    angular.module('app')
      .config(config)
      .controller('sliderCtrl', sliderCtrl);
  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
    .state('root.slider', {
      abstract:true,
      url: '/slider',
      data: {
        title: 'slider'
      },
      views: {
        '@':{
          templateUrl: 'app/partials/content.tpl.html',
          controller: 'sliderCtrl as slider'
        }
      }
    })
    .state('root.slider.page', {
        url: '',
        views: {
          'content@root.slider': {
            templateUrl: 'app/slider/slider.tpl.html'
          }
        }
      });

  }

  /**
   * @name  sliderCtrl
   * @description Controller
   */
  function sliderCtrl() {
    this.images = [{
      src: 'img1.png',
      title: 'Pic 1'
    }, {
      src: 'img2.jpg',
      title: 'Pic 2'
    }, {
      src: 'img3.jpg',
      title: 'Pic 3'
    }, {
      src: 'img4.png',
      title: 'Pic 4'
    }, {
      src: 'img5.png',
      title: 'Pic 5'
    }];
  }

})();
