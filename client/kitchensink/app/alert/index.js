(function() {
    'use strict';

    /**
     * @name  config
     * @description config block
     */
    function config($stateProvider) {
        $stateProvider
            .state('root.alert', {
                abstract: true,
                url: '/alert',
                data: {
                    title: 'alert'
                },
                views: {
                    '@': {
                        templateUrl: 'app/partials/content.tpl.html',
                        controller: 'alertCtrl as alert'
                    }
                }
            })
            .state('root.alert.page', {
                url: '',
                views: {
                    'content@root.alert': {
                        templateUrl: 'app/alert/alert.tpl.html'
                    },
                    'runtime@root.alert': {
                        templateUrl: 'app/alert/runC.tpl.html'
                    }
                }
            });
    }

    /**
     * @name  alertCtrl
     * @description Controller
     */
    function alertCtrl(message) {
        var vm = this;
        vm.text = 'text for content';
        vm.showModal = function() {
            message.activate({ 'text': vm.text });
        };

    }

    angular.module('app')
        .config(config)
        .controller('alertCtrl', alertCtrl);
})();
