angular.module('app')
    .factory('message', function(modalFactory) {
        return modalFactory({
            controller: 'myalertCtrl',
            controllerAs: 'modal',
            templateUrl: 'app/alert/modal.html'
        });
    })
    .controller('myalertCtrl', function(message) {
        console.log(message);
        console.log(this);
        this.deactivate = function() {
            message.deactivate();
        }
    })
