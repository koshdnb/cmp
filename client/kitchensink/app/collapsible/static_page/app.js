(function () {
  'use strict';

  angular
    .module('accordion', ['common.accordion','angular-debug-bar', 'common.ngPerformance', 'ngSanitize', 'ngAria'])
    .controller('AccordionCtrl', function () {
      this.panels = [
        {
          "title": "Valentino Rossi",
          "content": "Entra nel mondiale nel 1996 ed è su..."
        },
        {
          "title": "Jorge Lorenzo",
          "content": "Inizia la sua carriera mondiale nel 2002 ..."
        },
        {
          "title": "Marc Marchez",
          "content": "È il talento piú straodinario del panorama ..."
        }
      ];
    })
})();
