(function() {
    'use strict';

    /**
     * @name  config
     * @description config block
     */
    function config($stateProvider) {

        $stateProvider
            .state('root.collapsible', {
                abstract: true,
                data: {
                    title: 'collapsible'
                },
                url: '/collapsible',
                views: {
                    '@': {
                        templateUrl: 'app/partials/content.tpl.html',
                        controller: 'AccordionCtrl as ctrl'
                    },
                    'layout@root.collapsible': {
                        templateUrl: 'app/collapsible/layout.tpl.html'
                    },
                }
            })
            .state('root.collapsible.panels', {
                url: '/panels',
                views: {
                    'content@root.collapsible': {
                        templateUrl: 'app/collapsible/panels.tpl.html'
                    },
                    'runtime@root.collapsible': {
                        templateUrl: 'app/collapsible/runP.tpl.html'
                    }
                }
            })
            .state('root.collapsible.accordion', {
                url: '/accordion',
                views: {
                    'content@root.collapsible': {
                        templateUrl: 'app/collapsible/accordion.tpl.html'
                    },
                    'runtime@root.collapsible': {
                        templateUrl: 'app/collapsible/runC.tpl.html'
                    }
                }
            });

    }

    /**
     * @name  AccordionCtrl
     * @description Controller
     */
    function AccordionCtrl() {
        this.panel1 = { title: 'Panel 1', content: 'body' };

        this.panel2 = { title: 'Panel 2', content: 'body' };

        this.panels = [
            { title: 'Valentino Rossi', content: "Entra nel mondiale nel 1996 ed è su..." },
            { title: 'Jorge Lorenzo', content: "Inizia la sua carriera mondiale nel 2002 ..." },
            { title: 'Marc Marchez', content: "È il talento piú straodinario del panorama ..." },
        ];
    }

    angular.module('app')
        .config(config)
        .controller('AccordionCtrl', AccordionCtrl);
})();
