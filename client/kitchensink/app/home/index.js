(function() {
    'use strict';

    /**
     * @name  config
     * @description config block
     */
    function config($stateProvider) {
        $stateProvider
            .state('root.home', {
                abstract: true,
                url: '/',
                data: {
                    title: 'select component'
                },
                views: {
                    '@': {
                        templateUrl: 'app/partials/content.tpl.html',
                        controller: 'HomeCtrl as home'
                    }
                }
            })
            .state('root.home.page', {
                url: '',
                views: {
                    'content@root.home': {
                        templateUrl: 'app/home/home.tpl.html'
                    }
                }
            });
    }

    /**
     * @name  HomeCtrl
     * @description Controller
     */
    function HomeCtrl(message) {
        var home = this;
    }

    angular.module('app')
        .config(config)
        .controller('HomeCtrl', HomeCtrl);
})();
