(function() {
    'use strict';

    function config($stateProvider, $urlRouterProvider, $logProvider, $httpProvider) {
        $urlRouterProvider.otherwise('/');
        $logProvider.debugEnabled(true);
        $stateProvider
            .state('root', {
                views: {
                    // '':{
                    //   templateUrl: 'app/partials/content.tpl.html'
                    // },
                    'header': {
                        templateUrl: 'app/partials/header/header.tpl.html',
                        controller: 'HeaderCtrl'
                    },
                    'footer': {
                        templateUrl: 'app/partials/footer/footer.tpl.html',
                        controller: 'FooterCtrl'
                    }
                }
            });
    }

    function MainCtrl($log) {
        $log.debug('MainCtrl laoded!');
    }

    function run($log, $rootScope, $state) {
        $rootScope.state = $state;
        $log.debug('App is running!');
    }

    angular.module('app', [
            'ngAnimate',
            'ui.router',
            'ngAria',
            'ngSanitize',
            'common.htmlInclude',
            'common.accordion',
            'common.helpers.timer',
            'common.slider',
            'common.modal',
            'common.carousel',
            'common.ngPerformance',
            'angular-debug-bar'
            //  'templates'
        ])
        .config(config)
        .run(run)
        .controller('MainCtrl', MainCtrl)
        .value('version', '1.1.0');
})();
