(function() {
  'use strict';

  function footerCtrl($log) {
    $log.debug('Footer loaded');
  }

  angular.module('app')
    .controller('FooterCtrl', footerCtrl);
})();
