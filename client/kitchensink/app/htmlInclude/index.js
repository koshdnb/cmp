(function() {
  'use strict';

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
    .state('root.htmlInclude', {
      abstract:true,
      url: '/html-include',
      views: {
        '@':{
          templateUrl: 'app/partials/content.tpl.html',
          controller: 'includeCtrl as ctrl'
        }
      }
    })
    .state('root.htmlInclude.page', {
        url: '',
        views: {
          'content@root.htmlInclude': {
            templateUrl: 'app/htmlInclude/htmlInclude.tpl.html'
          }
        }
      });
  }

  /**
   * @name  HomeCtrl
   * @description Controller
   */
  function includeCtrl() {
    this.arr = [];
    this.time=0;
    for(var i = 0; i < 5000; i++){

      this.arr.push({title:''});
    }
  }

  angular.module('app')
    .config(config)
    .controller('includeCtrl', includeCtrl);
})();
