(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/vendor/slider.tpl.html",
    "<h3>Slider</h3>\n" +
    "<pre>\n" +
    "  &lt;slider&gt;\n" +
    "    &lt;slide&gt; &lt;/slide&gt;\n" +
    "    &lt;slide&gt; &lt;/slide&gt;\n" +
    "    &lt;slide&gt; &lt;/slide&gt;\n" +
    "  &lt;/slider&gt;\n" +
    "</pre>\n" +
    "<div style=\"width:600px;height:400px;\">\n" +
    "  <slider>\n" +
    "    <slide ng-repeat=\"image in slider.images\">\n" +
    "      <img ng-src=\"assets/img/{{image.src}}\">\n" +
    "    </slide>\n" +
    "  </slider>\n" +
    "\n" +
    "  <panel title=\"'second header'\">\n" +
    "    <h4 class=\"card-title\">Special title treatment</h4>\n" +
    "    <p class=\"card-text\">With supporting text below as a natural lead-in to additional content.</p>\n" +
    "    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>\n" +
    "    <slider>\n" +
    "      <slide ng-repeat=\"image in slider.images\">\n" +
    "        <img ng-src=\"assets/img/{{image.src}}\">\n" +
    "      </slide>\n" +
    "    </slider>\n" +
    "  </panel>\n" +
    "</div>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/accordion/accordion.tpl.html",
    "<h3>collapsable panel</h3>\n" +
    "<section class=\"island\">\n" +
    "  <h4>Panel</h4>\n" +
    "\n" +
    "  <pre>\n" +
    "        &lt;panel title=\"\"&gt;\n" +
    "            {{content}}\n" +
    "        &lt;/panel&gt;\n" +
    "    </pre>\n" +
    "\n" +
    "  <panel title=\"ctrl.panel1.title\">{{ctrl.panel1.content}}</panel>\n" +
    "\n" +
    "  <panel title=\"'second header'\">\n" +
    "    <h4 class=\"card-title\">Special title treatment</h4>\n" +
    "    <p class=\"card-text\">With supporting text below as a natural lead-in to additional content.</p>\n" +
    "    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>\n" +
    "  </panel>\n" +
    "</section>\n" +
    "<section class=\"island\">\n" +
    "  <h4>Accordion</h4>\n" +
    "  <pre>\n" +
    "       &lt;accordion data=\"ctrl.panels\"&gt;\n" +
    "         &lt;panel item=\"p\" ng-repeat=\"p in ctrl.panels\"&gt;&lt;/panel&gt;\n" +
    "       &lt;/accordion&gt;\n" +
    "     </pre>\n" +
    "\n" +
    "  <accordion>\n" +
    "    <panel title=\"p.title\" ng-repeat=\"p in ctrl.panels\">\n" +
    "      {{p.content}}\n" +
    "    </panel>\n" +
    "  </accordion>\n" +
    "</section>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/alert/alert.tpl.html",
    "<h3>alert with message</h3>\n" +
    "\n" +
    "<pre>\n" +
    "  angular.factory('factoryName', function (modalFactory) {\n" +
    "    return modalFactory({\n" +
    "      controller: '',\n" +
    "      controllerAs: '',\n" +
    "      templateUrl: ''\n" +
    "    });\n" +
    "  })\n" +
    "</pre>\n" +
    "then anywhere:\n" +
    "<pre>\n" +
    "  factoryName.activate();\n" +
    "</pre>\n" +
    "<h2>example</h2>\n" +
    "current template:\n" +
    "<pre>\n" +
    "  &lt;div class=&quot;modal demo-modal&quot;&gt;\n" +
    "    &lt;h3&gt;alert&lt;/h3&gt;\n" +
    "    &lt;a class=&quot;demo-modal-close&quot; href ng-click=&quot;modal.deactivate()&quot;&gt;x&lt;/a&gt;\n" +
    "    &lt;div ng-bind=&quot;text&quot;&gt;&lt;/div&gt;\n" +
    "  &lt;/div&gt;\n" +
    "</pre>\n" +
    "<h5>message for alert: </h5>\n" +
    "<textarea ng-model=\"alert.text\"></textarea>\n" +
    "\n" +
    "<div>\n" +
    "  <button ng-click=\"alert.showModal()\">Show alert</button>\n" +
    "</div>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/home/home.tpl.html",
    "\n" +
    "<h3>intro page</h3>\n" +
    "<p>all components can be mixed and nested without any limitation </p>\n" +
    "<p>for example:</p>\n" +
    "<pre>\n" +
    "  &lt;panel title=&quot;&apos;slider in panel&apos;&quot;&gt;\n" +
    "    &lt;slider&gt;\n" +
    "      &lt;slide&gt;\n" +
    "        &lt;p&gt;first slide&lt;/p&gt;\n" +
    "\n" +
    "        &lt;accordion&gt;\n" +
    "          &lt;panel title=&quot;&apos;included panel&apos;&quot;&gt;\n" +
    "            &lt;h4 class=&quot;card-title&quot;&gt;first&lt;/h4&gt;\n" +
    "            &lt;p class=&quot;card-text&quot;&gt;With supporting text below as a natural lead-in to additional content.&lt;/p&gt;\n" +
    "          &lt;/panel&gt;\n" +
    "          &lt;panel title=&quot;&apos;included second panel&apos;&quot;&gt;\n" +
    "            &lt;h4 class=&quot;card-title&quot;&gt;second&lt;/h4&gt;\n" +
    "            &lt;p class=&quot;card-text&quot;&gt;With supporting text below as a natural lead-in to additional content.&lt;/p&gt;\n" +
    "          &lt;/panel&gt;\n" +
    "        &lt;/accordion&gt;\n" +
    "      &lt;/slide&gt;\n" +
    "      &lt;slide&gt;\n" +
    "        &lt;p&gt;second slide&lt;/p&gt;\n" +
    "        &lt;img src=&quot;assets/img/img1.png&quot; alt=&quot;&quot;&gt;\n" +
    "      &lt;/slide&gt;\n" +
    "      &lt;slide&gt;\n" +
    "        &lt;p&gt;third slide&lt;/p&gt;\n" +
    "      &lt;/slide&gt;\n" +
    "    &lt;/slider&gt;\n" +
    "&lt;/panel&gt;\n" +
    "\n" +
    "</pre>\n" +
    "<panel title=\"'slider in panel'\">\n" +
    "  <div style=\"position:relative; height:500px;padding-top:10px;\">\n" +
    "    <slider>\n" +
    "      <slide>\n" +
    "        <p>first slide</p>\n" +
    "\n" +
    "        <accordion>\n" +
    "          <panel title=\"'included panel'\">\n" +
    "            <h4 class=\"card-title\">first</h4>\n" +
    "            <p class=\"card-text\">With supporting text below as a natural lead-in to additional content.</p>\n" +
    "          </panel>\n" +
    "          <panel title=\"'included second panel'\">\n" +
    "            <h4 class=\"card-title\">second</h4>\n" +
    "            <p class=\"card-text\">With supporting text below as a natural lead-in to additional content.</p>\n" +
    "          </panel>\n" +
    "        </accordion>\n" +
    "      </slide>\n" +
    "      <slide>\n" +
    "        <p>second slide</p>\n" +
    "        <img src=\"assets/img/img1.png\" alt=\"\">\n" +
    "      </slide>\n" +
    "      <slide>\n" +
    "        <p>third slide</p>\n" +
    "      </slide>\n" +
    "    </slider>\n" +
    "  </div>\n" +
    "</panel>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/htmlInclude/htmlInclude.tpl.html",
    "<script type=\"text/ng-template\" id=\"static.html\">\n" +
    "  <h3>insertion {{$index}}</h3>\n" +
    "  <p><strong>Creating a New UI Common Component</strong></p>\n" +
    "</script>\n" +
    "\n" +
    "<h4>fast include static template</h4>\n" +
    "<h5>demo of rendering template inside ng-repeat</h5>\n" +
    "<p>template:</p>\n" +
    "<pre>\n" +
    "  &lt;h3&gt;insertion {{$index}}&lt;/h3&gt;\n" +
    "  &lt;p&gt;&lt;strong&gt;Creating a New UI Common Component&lt;/strong&gt;&lt;/p&gt;\n" +
    "</pre>\n" +
    "<p>count:5000</p>\n" +
    "\n" +
    "<p>execution time: {{ctrl.time}}</p>\n" +
    "\n" +
    "<section ng-timeit=\"ctrl.time\">\n" +
    "  <div ng-repeat=\"i in ctrl.arr track by $index\">\n" +
    "    <div html-include=\"static.html\"></div>\n" +
    "    <!-- <ng-include src=\"'static.html'\"></ng-include> -->\n" +
    "  </div>\n" +
    "</section>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/partials/content.tpl.html",
    "<div class=\"pure-g island\">\n" +
    "  <div class=\"pure-u-1 pure-u-lg-1-3 island\">\n" +
    "    <div html-include=\"src/app/partials/menu.tpl.html\"></div>\n" +
    "  </div>\n" +
    "  <div class=\"pure-u-1 pure-u-lg-2-3 island\">\n" +
    "    <!-- <h2>demo</h2> -->\n" +
    "    <div ui-view=\"content\"></div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/partials/menu.tpl.html",
    "<ul class=\"pure-menu\">\n" +
    "  <li><a href=\"#/accordion\">accordion</a></li>\n" +
    "  <li><a href=\"#/html-include\">html include</a></li>\n" +
    "  <li><a href=\"#/slider\">slider</a></li>\n" +
    "  <li><a href=\"#/alert\">alert</a></li>\n" +
    "</ul>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/slider/slider.tpl.html",
    "<h3>Slider</h3>\n" +
    "<pre>\n" +
    "  &lt;slider&gt;\n" +
    "  &lt;slide ng-repeat=&quot;image in slider.images&quot;&gt;\n" +
    "    &lt;img ng-src=&quot;assets/img/{{image.src}}&quot;&gt;\n" +
    "  &lt;/slide&gt;\n" +
    "&lt;/slider&gt;\n" +
    "</pre>\n" +
    "model: <pre>{{slider.images | json}}</pre>\n" +
    "<div style=\"position:relative;width:600px;height:400px;\">\n" +
    "  <slider>\n" +
    "    <slide ng-repeat=\"image in slider.images\">\n" +
    "      <img ng-src=\"assets/img/{{image.src}}\">\n" +
    "    </slide>\n" +
    "  </slider>\n" +
    "</div>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/partials/footer/footer.tpl.html",
    "<div class=\"pure-g\">\n" +
    "  <div class=\"pure-u-1 text-center\">\n" +
    "    <p>&copy; 2016 </p>\n" +
    "  </div>\n" +
    "</div>\n" +
    "");
}]);
})();

(function(module) {
try { module = angular.module("templates"); }
catch(err) { module = angular.module("templates", []); }
module.run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("src/app/partials/header/header.tpl.html",
    "<div class=\"pure-menu pure-menu-open pure-menu-horizontal\">\n" +
    "  <a class=\"pure-menu-heading\" href=\"#/\">kitchensink</a>\n" +
    "\n" +
    "</div>\n" +
    "");
}]);
})();

