(function() {
    'use strict';

    angular.module('common.accordion', [])
        .directive('accordion', function() {

            return {
                restrict: 'E',
                scope: {},
                bindToController: true,
                controllerAs: 'ctrl',
                controller: function ctrl() {
                    this.data = [];

                    this.addPane = function(panel) {
                        this.data.push(panel);
                    };

                    this.closeAll = function() {
                        angular.forEach(this.data, function(value, key) {
                            value.opened = false;
                        });

                    };
                }
            };

        })

    // Panel directive
    .directive('panel', function($parse) {

        return {
            restrict: 'E',
            require: '?^accordion',
            transclude: true,
            // replace: true,
            scope: {
                title: '=?'
            },
            template: [
                '<div class="panel panel-default" >',
                '<div class="panel-heading" ng-click="toggle();" ng-bind-html="title"></div>',
                '<div class="panel-body" ng-show="opened" ng-transclude></div>',
                '</div>'
            ].join(''),
            link: function(scope, el, attrs, ctrl) {

                scope.opened = false;

                if (ctrl) {
                    ctrl.addPane(scope);
                }

                scope.toggle = function() {
                    if (ctrl) {
                        ctrl.closeAll();
                    }
                    scope.opened = !scope.opened;
                };
            }
        };
    });

})();
