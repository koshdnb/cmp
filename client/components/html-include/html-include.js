angular.module('common.htmlInclude', [])
.directive('htmlInclude', [
  '$templateCache',
  function($templateCache) {
    return {
      restrict: 'A',
      priority: 400,
      compile: function(element, attrs){
        var templateName = attrs.htmlInclude;
        var template = $templateCache.get(templateName);
     	  if(angular.isUndefined(template)){
          throw new Error('htmlInclude: unknown template ' + templateName);
        }

        element.html(template);
      }
    };
  }
]);
