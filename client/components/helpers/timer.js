(function() {
  'use strict';

  angular.module('common.helpers.timer',[])
    .directive('ngTimeit', [
    '$parse',
    '$timeout',
    function($parse, $timeout){
      return {
        restrict: 'A',
        link: function(scope, element, attrs){
          var modelFn = $parse(attrs.ngTimeit);
          var then = new Date();
          $timeout(function(){
            var now = new Date();
            modelFn.assign(scope, (now - then) / 1000.0);
          });
        }
      };
    }
  ]);

})();
