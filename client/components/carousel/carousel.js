(function () {
  'use strict';

  angular
    .module('common.carousel', [])
    .directive('krnCarousel', CarouselDirective);

  function CarouselDirective() {

    return {
      restrict        : 'E',
      scope           : {
        index         : '=',
        array         : '=',
        onNextItemShow: '&?',
        onPrevItemShow: '&?',
        wrap          : '=?',
        caption       : '@?'
      },
      template        : [
        '<nav class="carousel" aria-describedby="caption" role="region">' +
          '<span id="caption" class="sr-only" ng-bind="carousel.caption"></span> ' +
          '<button type="button" class="previous btn btn-default" ng-click="carousel.previousItem($event)">' +
            '<i class="glyphicon glyphicon-chevron-left"></i>' +
            '<span class="sr-only">Previous Element</span>' +
          '</button>' +
          '<div class="count" role="status" ng-bind="carousel.counterText" aria-live="polite" aria-atomic="true"></div>' +
          '<button type="button" class="next btn btn-default" ng-click="carousel.nextItem($event)">' +
            '<i class="glyphicon glyphicon-chevron-right"></i>' +
            '<span class="sr-only">Next Element</span>' +
          '</button>' +
        '</nav>'
      ].join(''),
      controllerAs    : 'carousel',
      bindToController: true,
      controller      : function ($scope, $element) {
        var vm = this;
        vm.counterText;

        var currentIndex,
          total   = vm.array.length,
          nextBtn = $element.find('button.next'),
          prevBtn = $element.find('button.previous');

        init();

        $scope.$watchGroup([
          'carousel.index',
          'carousel.array',
          'carousel.wrap'
        ], function (newValues, oldValues) {
          if (newValues[0] !== undefined && newValues[1] !== undefined && newValues[2] !== undefined) {
            updateButtonsEnabledState();
          }
          if (newValues[0] !== oldValues[0]) {
            currentIndex = newValues[0] + 1;
            vm.counterText = currentIndex+ ' of '+ total;
          }
        });

        vm.nextItem = function (event) {
          if (nextBtn.hasClass('disabled')) {
            event.preventDefault();
            return;
          }

          if (vm.array.length === vm.index + 1 && vm.wrap) {
            vm.index = 0;
          } else {
            vm.index++;
          }

          if (angular.isFunction(vm.onNextItemShow)) {
            vm.onNextItemShow()(vm.index);
          }
        };

        vm.previousItem = function (event) {
          if (prevBtn.hasClass('disabled')) {
            event.preventDefault();
            return;
          }
          if (vm.index === 0 && vm.wrap) {
            vm.index = vm.array.length - 1;
          } else {
            vm.index--;
          }

          if (angular.isFunction(vm.onPrevItemShow)) {
            vm.onPrevItemShow()(vm.index);
          }
        };

        function init() {

          if (angular.isUndefined(vm.wrap)) {
            vm.wrap = true;
          }

          if (angular.isUndefined(vm.caption)) {
            vm.caption = 'You\'re on Carousel element, that contains previous and next buttons for navigation between items'
          }

          if (vm.array.length === 0) {
            vm.index = vm.array.length;
          }

          currentIndex = (vm.array.length === 0) ? vm.index : vm.index + 1;

          vm.counterText =  currentIndex+ ' of '+ total;
        }

        function updateButtonsEnabledState() {
          if (!vm.wrap) {
            if (vm.array.length <= 1) {
              setDisabledState(nextBtn);
              setDisabledState(prevBtn);
            } else if (vm.array.length === vm.index + 1) {
              setDisabledState(nextBtn);
              removeDisabledState(prevBtn);
            } else if (vm.index === 0) {
              removeDisabledState(nextBtn);
              setDisabledState(prevBtn);
            } else {
              removeDisabledState(nextBtn);
              removeDisabledState(prevBtn);
            }
          } else if (vm.array.length <= 1) {
            setDisabledState(nextBtn);
            setDisabledState(prevBtn);
          } else {
            removeDisabledState(nextBtn);
            removeDisabledState(prevBtn);
          }
        }

        function setDisabledState(element) {
          element.addClass('disabled').attr('aria-disabled', true);
        }

        function removeDisabledState(element) {
          element.removeClass('disabled').attr('aria-disabled', false);
        }
      }

    };
  }

})();






