angular.module('common.panel', [])

// Collapsable Panel Component
.component('cPanel', {
  bindings: {
    title: '='
  },
  template: [
    '<div class="card">',
      '<div class="card-header" ng-click="panel.toggle()"> {{panel.title}} </div>',
      '<div class="card-block" ng-class="{hide: !panel.opened}" ng-transclude=""></div>',
    '</div>'
  ].join(''),
  controller: function() {
    this.opened = false;
    this.toggle = function () {
      this.opened = !this.opened;
    }
  }
})
