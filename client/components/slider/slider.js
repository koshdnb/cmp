angular.module('common.slider', ['ngAnimate'])
  .directive('slider', function($timeout) {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      scope: {
        // images: '='
      },
      controllerAs: 'ctrl',
      controller: function commonSliderCtrl() {
        var ctrl=this;

        ctrl.currentIndex = 0;
        ctrl.data = [];

  			ctrl.addSlide = function(slide) {
          ctrl.data.length == 0 ? slide.visible=true:null;
          ctrl.data.push(slide);
        }
        ctrl.next = function() {
          ctrl.data.forEach(function(slide){
           		slide.visible=false;
           	});
          ctrl.currentIndex < ctrl.data.length - 1 ? ctrl.currentIndex++ : ctrl.currentIndex = 0;
          ctrl.data[ctrl.currentIndex].visible=true;
        };

        ctrl.prev = function() {
          ctrl.data.forEach(function(slide){
           		slide.visible=false;
           	});
        ctrl.currentIndex > 0 ? ctrl.currentIndex-- : ctrl.currentIndex = ctrl.data.length - 1;
        ctrl.data[ctrl.currentIndex].visible=true;
        };
      },
      template: [
        '<div>',
            '<div class="slider" ng-transclude></div>',
            '<div class="arrows">',
              '<span ng-click="ctrl.prev()">',
                '<img src="assets/img/left-arrow.png">',
              '</span>',
              '<span ng-click="ctrl.next()">',
                '<img src="assets/img/right-arrow.png">',
              '</span>',
            '</div>',
        '</div>'
      ].join('')
    }
  })
  .directive('slide', function($timeout) {
    return {
      restrict: 'E',
      require: '^slider',
       scope:{},
      replace: true,
      transclude: true,
      link: function(scope, elem, attrs, ctrl) {
        scope.visible = false;
        if (ctrl) {
          ctrl.addSlide(scope);
        }

      },
      template: [
        '<div class="slide" ng-show="visible" ng-transclude>',
        '</div>'
      ].join('')
    }
  })
